BUILDDIR:=build

all: build

serve:
	php -S localhost:8080 -t $(BUILDDIR)

build: $(shell find static src)
	mkdir -p $(BUILDDIR)
	yarn tsc
	cp static/* $(BUILDDIR)

clean:
	rm -rf $(BUILDDIR)
