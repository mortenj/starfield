/**
 * 
 *  starfield.js
 *  by morten@bzzt.no
 * 
 *  - a simple exploration of the HTML 5 canvas element
 * 
 */

class Star {
    max_distance = 5;
    max_x: number;
    max_y: number;
    posX: number;
    posY: number;
    distance: number;
    justInitialized: boolean = false;

    constructor(max_x, max_y) {
        this.max_x = max_x
        this.max_y = max_y
        this.initialize(true);
    }

    initialize(first: boolean = false) {
        if(first) {
            this.posX = Math.round(Math.random()*this.max_x);
            this.justInitialized = true;
            //console.log("pos: "+this.posX);
        } else {
            this.posX = 0;
        }
        this.posY = Math.round(Math.random()*this.max_y);
        // Speed and color are derived from distance
        this.distance = Math.random() * this.max_distance;
    }

    get size(): number {
        return Math.abs((this.max_distance - this.distance) / 4)+1;
    }

    get color(): string {
        let r = Math.round(200 - (this.distance*32)).toString(16);
        let g = Math.round(225 - (this.distance*32)).toString(16);
        let b = Math.round(255 - (this.distance*32)).toString(16);
        return '#' + r + g + b;
    }

    get speed(): number {
        // Pixels per millisecond. Distant stars are slower.
        return (this.max_distance - this.distance + 1)/30;
    }

    draw(ctx, period) {
        if(this.posX > this.max_x) {
            this.initialize();
        }
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.arc(this.posX, this.posY, this.size, 0, 2*Math.PI);
        ctx.fill();

        if(!this.justInitialized) {
            this.posX += period * this.speed;
        } else {
            this.justInitialized = false;
        }
    }

}

class Starfield {
    stars = Array<Star>();
    ctx = null;
    height = 0;
    width = 0;
    bgColor = 'white';

    constructor(canvas, num_stars) {
        this.ctx = canvas.getContext("2d");
        this.height = canvas.height;
        this.width = canvas.width;

        let bodyStyle = window.getComputedStyle(document.body, null);
        this.bgColor = bodyStyle.backgroundColor;

        // Stars
        for (let i = 0; i < num_stars; i++) {
            this.stars.push(new Star(this.width, this.height));
        }
    }

    distributeStars() {
        this.stars.forEach(
            (star) => { 
                star.initialize(true); 
            }
        );
    }

    drawFrame(period: number) {
        this.ctx.fillStyle = this.bgColor;
        this.ctx.fillRect(0, 0, this.width, this.height);
    
        this.stars.forEach(star => {
            star.draw(this.ctx, period);
        });
    }
}

console.log("We're running!");

function getStarfieldElement(): HTMLCanvasElement {
    const canvases = document.getElementsByTagName("canvas");
    for(let canvas of canvases) {
        if(canvas.id == "starfield") return canvas;
    }
}

let canvas = getStarfieldElement();
let star_density = 16;
let num_stars = Math.round(canvas.width * canvas.height * star_density/100000);
let sf = new Starfield(canvas, num_stars);

let isRunning = true;
function togglePlay() {
    isRunning = !isRunning;
    console.log("Starfield is running: " + isRunning);
}

canvas.addEventListener('click', togglePlay);

document.addEventListener("visibilitychange", function() {
    if (document.visibilityState === 'visible') {
        console.log("became visible. redistributing stars...");
        sf.distributeStars();
    }
  }
);

let last: DOMHighResTimeStamp;
function animate(time: DOMHighResTimeStamp) {
    if(last!==undefined) {
        if(isRunning) sf.drawFrame(time-last);
    }

    last=time;
    window.requestAnimationFrame(animate);
}

window.requestAnimationFrame(animate);
